# Presentazione Voxxed 2017

## Preparazione 

```
git clone git@gitlab.com:blackdev1l/voxxed2k17.git
cd voxxed2k17
npm install
npm start
```

## Aggiungere slides
Creare un file `nome.html`

scrivere le slides nel file seguendo le guide lines presenti nel [README](https://github.com/hakimel/reveal.js) della repo originale 

includerlo nel div "slides" nel seguente modo:
```<section data-external-replace="nome.html"></section>```
